using NUnit.Framework;
using MyStringCalc;

namespace MyStringCalcTests
{
    public class CheckTests
    {
        private MathExpressionOperator[] _operators;
        private (char, char)[] _brackets;
        private char[] _separators;

        [SetUp]
        public void Setup()
        {
            _operators = new MathExpressionOperator[]
            {
                new MathExpressionOperator('+', 1, new AdditionOperation()),
                new MathExpressionOperator('-', 1, new SubtractionOperation()),
                new MathExpressionOperator('*', 2, new MultiplicationOperation()),
                new MathExpressionOperator('/', 2, new DivisionOperation())
            };

            _brackets = new (char, char)[] { ('(', ')') };
            _separators = new char[] { '.', ',' };
        }

        [Test]
        public void TestRuleNoSpaces()
        {
            string mathExpression = "2 +3";
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleNoSpaces() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            Assert.IsFalse(parser.Check(mathExpression, out errorMessage));
        }

        [Test]
        public void TestRuleNoDoubleOperationSymbols()
        {
            string mathExpression = "2*+3";
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleNoDoubleOperationSymbols() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            Assert.IsFalse(parser.Check(mathExpression, out errorMessage));
        }

        [Test]
        public void TestRuleNoMoreThanOneSeparatorInNumber()
        {
            string[] mathExpressions = { "2.3.3", "2..3" };
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleNoMoreThanOneSeparatorInNumber() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            for (int i = 0; i < mathExpressions.Length; ++i)
                Assert.IsFalse(parser.Check(mathExpressions[i], out errorMessage));
        }

        [Test]
        public void TestRuleAllBracketsClosed()
        {
            string[] mathExpressions = { "(2+(3)", "(2+3))", ")2+3(" };
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleAllBracketsClosed() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            for (int i = 0; i < mathExpressions.Length; ++i)
                Assert.IsFalse(parser.Check(mathExpressions[i], out errorMessage));
        }

        [Test]
        public void TestRuleOnlyNumbersOrSpecialSymbols()
        {
            string mathExpression = "n+3";
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleOnlyNumbersOrSpecialSymbols() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            Assert.IsFalse(parser.Check(mathExpression, out errorMessage));
        }

        [Test]
        public void TestRuleNoOperationSymbolBeforeCloseBracket()
        {
            string mathExpression = "(2+3+)";
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleNoOperationSymbolBeforeCloseBracket() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            Assert.IsFalse(parser.Check(mathExpression, out errorMessage));
        }

        [Test]
        public void TestRuleBracketsMustBeFilled()
        {
            string mathExpression = "()";
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleBracketsMustBeFilled() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            Assert.IsFalse(parser.Check(mathExpression, out errorMessage));
        }

        [Test]
        public void TestRuleNoNumberSymbolBeforeOpenBracket()
        {
            string mathExpression = "2(2+3)";
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleNoNumberSymbolBeforeOpenBracket() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            Assert.IsFalse(parser.Check(mathExpression, out errorMessage));
        }

        [Test]
        public void TestRuleNoNumberSymbolBeforeCloseBracket()
        {
            string mathExpression = "(2+3)2";
            string errorMessage;
            IMathExpressionCheckRule[] rules = { new RuleNoNumberSymbolAfterCloseBracket() };
            MathExpressionParser parser = new MathExpressionParser(_operators, _brackets, _separators, rules);

            Assert.IsFalse(parser.Check(mathExpression, out errorMessage));
        }
    }
}