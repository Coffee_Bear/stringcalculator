﻿using NUnit.Framework;
using MyStringCalc;

namespace MyStringCalcTests
{
    public class CalculationTests
    {
        private MathExpressionOperator[] _operators;
        private (char, char)[] _brackets;
        private char[] _separators;
        private IMathExpressionCheckRule[] _checkRules;
        private MathExpressionParser _parser;

        private float CalculateExpression(string mathExpression)
        {
            BinaryTreeNode<string> mathExpressionAsTree = _parser.Parse(mathExpression);
            return MathExpression.Calculate(mathExpressionAsTree, _parser.operators, _parser.separators);
        }

        [SetUp]
        public void Setup()
        {
            _operators = new MathExpressionOperator[]
            {
                new MathExpressionOperator('+', 1, new AdditionOperation()),
                new MathExpressionOperator('-', 1, new SubtractionOperation()),
                new MathExpressionOperator('*', 2, new MultiplicationOperation()),
                new MathExpressionOperator('/', 2, new DivisionOperation())
            };

            _brackets = new (char, char)[] { ('(', ')') };
            _separators = new char[] { '.', ',' };
            _checkRules = new IMathExpressionCheckRule[]
                {
                new RuleNoSpaces(),
                new RuleNoDoubleOperationSymbols(),
                new RuleNoMoreThanOneSeparatorInNumber(),
                new RuleAllBracketsClosed(),
                new RuleOnlyNumbersOrSpecialSymbols(),
                new RuleNoOperationSymbolBeforeCloseBracket(),
                new RuleBracketsMustBeFilled(),
                new RuleNoNumberSymbolBeforeOpenBracket(),
                new RuleNoNumberSymbolAfterCloseBracket()
            };
            _parser = new MathExpressionParser(_operators, _brackets, _separators, _checkRules);
        }

        [Test(Description = "Math expression: 1")]
        public void Test1()
        {
            string mathExpression = "1";
            float answer = 1;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: -1")]
        public void Test2()
        {
            string mathExpression = "-1";
            float answer = -1;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: (1)")]
        public void Test3()
        {
            string mathExpression = "(1)";
            float answer = 1;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: (-1)")]
        public void Test4()
        {
            string mathExpression = "(-1)";
            float answer = -1;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 2+3")]
        public void Test5()
        {
            string mathExpression = "2+3";
            float answer = 5;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 2-3")]
        public void Test6()
        {
            string mathExpression = "2-3";
            float answer = -1;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 2*3")]
        public void Test7()
        {
            string mathExpression = "2*3";
            float answer = 6;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 6/3")]
        public void Test8()
        {
            string mathExpression = "6/3";
            float answer = 2;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: -2+3")]
        public void Test9()
        {
            string mathExpression = "-2+3";
            float answer = 1;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: -(2+3)")]
        public void Test10()
        {
            string mathExpression = "-(2+3)";
            float answer = -5;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 1.5+0.5")]
        public void Test11()
        {
            string mathExpression = "1.5+0.5";
            float answer = 2;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 1,5+0,5")]
        public void Test12()
        {
            string mathExpression = "1.5+0.5";
            float answer = 2;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 1.5+0,5")]
        public void Test13()
        {
            string mathExpression = "1.5+0.5";
            float answer = 2;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 2+3*2")]
        public void Test14()
        {
            string mathExpression = "2+3*2";
            float answer = 8;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 3*2+2")]
        public void Test15()
        {
            string mathExpression = "3*2+2";
            float answer = 8;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: (2+3)*2")]
        public void Test16()
        {
            string mathExpression = "(2+3)*2";
            float answer = 10;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 2*(2+3)")]
        public void Test17()
        {
            string mathExpression = "2*(2+3)";
            float answer = 10;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 5-(2+3)")]
        public void Test18()
        {
            string mathExpression = "5-(2+3)";
            float answer = 0;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: (12-2)*(-2-3)")]
        public void Test19()
        {
            string mathExpression = "(12-2)*(-2-3)";
            float answer = -50;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }

        [Test(Description = "Math expression: 10-((2+3)*2-2)")]
        public void Test20()
        {
            string mathExpression = "10-((2+3)*2-2)";
            float answer = 2;

            float calculationResult = CalculateExpression(mathExpression);
            Assert.AreEqual(answer, calculationResult);
        }
    }
}
