﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;

namespace MyStringCalc
{
    public class MathExpression
    {
        private static CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();

        public static float Calculate(BinaryTreeNode<string> mathExpression, MathExpressionOperator[] operators, char[] separators)
        {
            int signIndex;
            if(MathExpressionUtilities.IsSign(mathExpression.data[0], operators, out signIndex))
            {
                float leftNodeValue = 0;
                float rightNodeValue = 0;

                if (mathExpression.leftNode != null)
                    leftNodeValue = Calculate(mathExpression.leftNode, operators, separators);

                if (mathExpression.rightNode != null)
                    rightNodeValue = Calculate(mathExpression.rightNode, operators, separators);

                return operators[signIndex].operation.Calculate(leftNodeValue, rightNodeValue);
            }
            else
            {
                float result = 0;
                bool parseSuccess = false;
                for (int i = 0; i < separators.Length; ++i)
                {
                    ci.NumberFormat.CurrencyDecimalSeparator = separators[i].ToString();
                    if (parseSuccess = float.TryParse(mathExpression.data, NumberStyles.Any, ci, out result))                    
                        break;        
                }

                if (!parseSuccess)
                    throw new FormatException(mathExpression.data);

                return result;
            }
        }
    }
}