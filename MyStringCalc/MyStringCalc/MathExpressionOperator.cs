﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyStringCalc
{
    public class MathExpressionOperator
    {
        public char symbol { get; private set; }
        public int priority { get; private set; }
        public IMathOperation operation { get; private set; }

        public MathExpressionOperator(char symbol, int priority, IMathOperation operation)
        {
            this.symbol = symbol;
            this.priority = priority;
            this.operation = operation;
        }
    }
}