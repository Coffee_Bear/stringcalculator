﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyStringCalc
{
    public enum Side
    {
        Left,
        Right,
        Unknown
    }

    public class BinaryTreeNode<T>
    {
        public BinaryTreeNode<T> parentNode { get; private set; }
        public BinaryTreeNode<T> leftNode { get; private set; }
        public BinaryTreeNode<T> rightNode { get; private set; }

        public T data { get; set; }

        public BinaryTreeNode(T data)
        {
            this.data = data;
        }

        public Side nodeSide => parentNode == null ? Side.Unknown: parentNode.leftNode == this ? Side.Left: Side.Right;
             
        private void SetParrent(BinaryTreeNode<T> node)
        {
            switch (nodeSide)
            {
                case Side.Left: parentNode.RemoveLeftNode(); break;
                case Side.Right: parentNode.RemoveRightNode(); break;
            };

            parentNode = node;
        }

        public void SetLeftNode(BinaryTreeNode<T> node)
        {
            if (leftNode != null)
                leftNode.parentNode = null;

            leftNode = node;

            if(leftNode!=null)            
                leftNode.SetParrent(this);            
        }

        public void SetRightNode(BinaryTreeNode<T> node)
        {
            if (rightNode != null)
                rightNode.parentNode = null;

            rightNode = node;

            if (rightNode != null)            
                rightNode.SetParrent(this);            
        }

        public BinaryTreeNode<T> RemoveLeftNode()
        {
            BinaryTreeNode<T> node = leftNode;
            SetLeftNode(null);
            return node;
        }

        public BinaryTreeNode<T> RemoveRightNode()
        {
            BinaryTreeNode<T> node = rightNode;
            SetRightNode(null);
            return node;
        }
    }
}