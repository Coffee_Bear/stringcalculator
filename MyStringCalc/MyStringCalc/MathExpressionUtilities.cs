﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyStringCalc
{
    class MathExpressionUtilities
    {
        public static bool IsSign(char symbol, MathExpressionOperator[] operators)
        {
            for (int i = 0; i < operators.Length; ++i)
            {
                if (operators[i].symbol == symbol)
                    return true;
            }

            return false;
        }

        public static bool IsSign(char symbol, MathExpressionOperator[] operators, out int operatorIndex)
        {
            operatorIndex = -1;
            for (int i = 0; i < operators.Length; ++i)
            {
                if (operators[i].symbol == symbol)
                {
                    operatorIndex = i;
                    return true;
                }
            }

            return false;
        }

        public static bool IsSign(char symbol, MathExpressionOperator[] operators, out MathExpressionOperator suitableOperator)
        {
            suitableOperator = null;
            for (int i = 0; i < operators.Length; ++i)
            {
                if (operators[i].symbol == symbol)
                {
                    suitableOperator = operators[i];
                    return true;
                }
            }

            return false;
        }

        public static bool IsBracket(char symbol, (char, char)[] brackets)
        {            
            for (int i = 0; i < brackets.Length; ++i)
            {
                if (brackets[i].Item1 == symbol || brackets[i].Item2 == symbol)
                    return true;
            }

            return false;
        }

        public static bool IsOpenBracket(char symbol, (char, char)[] brackets)
        {
            for (int i = 0; i < brackets.Length; ++i)
            {
                if (brackets[i].Item1 == symbol)
                    return true;
            }

            return false;
        }

        public static bool IsCloseBracket(char symbol, (char, char)[] brackets)
        {
            for (int i = 0; i < brackets.Length; ++i)
            {
                if (brackets[i].Item2 == symbol)
                    return true;
            }

            return false;
        }
          
        public static bool IsSeparator(char symbol, char[] separators)
        { 
            for (int i = 0; i < separators.Length; ++i)
            {
                if (separators[i] == symbol)
                    return true;
            }

            return false;
        }

        public static bool IsSpecialSymbol(char symbol, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {            
            return IsSign(symbol, operators) || IsBracket(symbol, brackets) || IsSeparator(symbol, separators);
        }
    }
}
