﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyStringCalc
{
    public interface IMathExpressionCheckRule
    {
        bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators);

        string GetErrorMessage();
    }

    public class RuleNoSpaces : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            for (int i = 0; i < stringToCheck.Length; ++i)
                if (stringToCheck[i] == ' ')
                    return false;

            return true;
        }

        public string GetErrorMessage() => "Ошибка: присутствуют пробелы в выражении";
    }

    public class RuleNoDoubleOperationSymbols : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] signs, (char, char)[] brackets, char[] separators)
        {
            for (int i = 1; i < stringToCheck.Length; ++i)
            {
                char previousSymbol = stringToCheck[i - 1];
                char currentSymbol = stringToCheck[i];
                if (MathExpressionUtilities.IsSign(currentSymbol, signs) && MathExpressionUtilities.IsSign(previousSymbol, signs))
                    return false;
            }

            return true;
        }

        public string GetErrorMessage() => "Ошибка: два знака операции подряд";
    }

    public class RuleNoMoreThanOneSeparatorInNumber : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            bool isSeparatorAlreadyWasBeenInThisNumber = false;

            for (int i = 0; i < stringToCheck.Length; ++i)
            {
                char currentSymbol = stringToCheck[i];
                if (MathExpressionUtilities.IsSeparator(currentSymbol, separators))
                {
                    if (isSeparatorAlreadyWasBeenInThisNumber)
                        return false;
                    else
                        isSeparatorAlreadyWasBeenInThisNumber = true;
                }
                else if (MathExpressionUtilities.IsSign(currentSymbol, operators) || MathExpressionUtilities.IsBracket(currentSymbol, brackets))
                {
                    isSeparatorAlreadyWasBeenInThisNumber = false;
                }
            }

            return true;
        }

        public string GetErrorMessage() => "Ошибка: более одного символа десятичного разделителя в числе";
    }

    public class RuleAllBracketsClosed : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            int[] bracketsClosed = new int[brackets.Length];
            char currentSymbol;

            for (int i = 0; i < stringToCheck.Length; ++i)
            {
                currentSymbol = stringToCheck[i];
                for (int j = 0; j < brackets.Length; ++j)
                {
                    if (bracketsClosed[j] < 0)
                        return false;

                    if (brackets[j].Item1 == currentSymbol)
                    {
                        ++bracketsClosed[j];
                        break;
                    }
                    else if (brackets[j].Item2 == currentSymbol)
                    {
                        --bracketsClosed[j];
                        break;
                    }
                }
            }

            for (int i = 0; i < bracketsClosed.Length; ++i)
                if (bracketsClosed[i] != 0)
                    return false;

            return true;
        }

        public string GetErrorMessage() => "Ошибка: не все скобки закрыты";
    }

    public class RuleOnlyNumbersOrSpecialSymbols : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            for (int i = 0; i < stringToCheck.Length; ++i)
                if (!(Char.IsNumber(stringToCheck[i]) || MathExpressionUtilities.IsSpecialSymbol(stringToCheck[i], operators, brackets, separators)))
                    return false;

            return true;
        }

        public string GetErrorMessage() => "Ошибка: выражение содержит недопустимый символ";
    }

    public class RuleNoOperationSymbolBeforeCloseBracket : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            for (int i = 1; i < stringToCheck.Length; ++i)
                if (MathExpressionUtilities.IsSign(stringToCheck[i - 1], operators) && MathExpressionUtilities.IsCloseBracket(stringToCheck[i], brackets))
                    return false;

            return true;
        }

        public string GetErrorMessage() => "Ошибка: выражение содержит знак операции перед закрывающей скобкой";
    }

    public class RuleBracketsMustBeFilled : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            for (int i = 1; i < stringToCheck.Length; ++i)
            {
                if (MathExpressionUtilities.IsCloseBracket(stringToCheck[i], brackets) && MathExpressionUtilities.IsOpenBracket(stringToCheck[i - 1], brackets))
                    return false;

                if (MathExpressionUtilities.IsOpenBracket(stringToCheck[i], brackets) && MathExpressionUtilities.IsCloseBracket(stringToCheck[i - 1], brackets))
                    return false;
            }

            return true;
        }

        public string GetErrorMessage() => "Ошибка: открывающая и закрывающая скобки не должны находиться рядом";
    }

    public class RuleNoNumberSymbolBeforeOpenBracket : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            for (int i = 1; i < stringToCheck.Length; ++i)
                if (MathExpressionUtilities.IsOpenBracket(stringToCheck[i], brackets)
                    && !MathExpressionUtilities.IsSign(stringToCheck[i - 1], operators)
                    && !MathExpressionUtilities.IsOpenBracket(stringToCheck[i-1], brackets))
                {                   
                    return false;
                }

            return true;
        }

        public string GetErrorMessage() => "Ошибка: выражение должно содержать знак операции перед открывающей скобкой";
    }

    public class RuleNoNumberSymbolAfterCloseBracket : IMathExpressionCheckRule
    {
        public bool Check(string stringToCheck, MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators)
        {
            for (int i = 1; i < stringToCheck.Length; ++i)
                if (MathExpressionUtilities.IsCloseBracket(stringToCheck[i - 1], brackets)
                    && !MathExpressionUtilities.IsSign(stringToCheck[i], operators)
                    && !MathExpressionUtilities.IsOpenBracket(stringToCheck[i], brackets))
                {
                    return false;
                }

            return true;
        }

        public string GetErrorMessage() => "Ошибка: выражение должно содержать знак операции после закрывающей скобкой";
    }
}