﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyStringCalc
{
    public interface IMathOperation
    {
        float Calculate(float arg1, float arg2);
    }

    public class AdditionOperation : IMathOperation
    {
        public float Calculate(float arg1, float arg2) => arg1 + arg2;
    }

    public class SubtractionOperation : IMathOperation
    {
        public float Calculate(float arg1, float arg2) => arg1 - arg2;
    }

    public class MultiplicationOperation : IMathOperation
    {
        public float Calculate(float arg1, float arg2) => arg1 * arg2;
    }

    public class DivisionOperation : IMathOperation
    {
        public float Calculate(float arg1, float arg2) => arg1 / arg2;
    }
}