﻿using System;

namespace MyStringCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            MathExpressionOperator[] operators =
                {
                new MathExpressionOperator ('+', 1, new AdditionOperation()),
                new MathExpressionOperator ('-', 1, new SubtractionOperation()),
                new MathExpressionOperator('*', 2, new MultiplicationOperation()),
                new MathExpressionOperator('/', 2, new DivisionOperation()) 
            };

            (char, char)[] brackets = { ('(', ')') };
            char[] separators = { '.', ',' };
            IMathExpressionCheckRule[] checkRules =
                {
                new RuleNoSpaces(),
                new RuleNoDoubleOperationSymbols(),
                new RuleNoMoreThanOneSeparatorInNumber(),
                new RuleAllBracketsClosed(),
                new RuleOnlyNumbersOrSpecialSymbols(),
                new RuleNoOperationSymbolBeforeCloseBracket(),
                new RuleBracketsMustBeFilled(),
                new RuleNoNumberSymbolBeforeOpenBracket(),
                new RuleNoNumberSymbolAfterCloseBracket()
            };
            MathExpressionParser parser = new MathExpressionParser(operators, brackets, separators, checkRules);

            string input;
            string errorMessage;
            BinaryTreeNode<string> mathExpression;
            while (true)
            {
                Console.WriteLine("Введите выражение:");
                input = Console.ReadLine();

                if (parser.Check(input, out errorMessage))
                {
                    mathExpression = parser.Parse(input);
                    Console.WriteLine("Ответ: {0}\n", MathExpression.Calculate(mathExpression, operators, separators));
                }
                else
                    Console.WriteLine(errorMessage);
            }
        }
    }
}
