﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyStringCalc
{
    public class MathExpressionParser
    {
        public MathExpressionOperator[] operators { get; private set; }
        public (char, char)[] brackets { get; private set; }
        public char[] separators { get; private set; }
        public IMathExpressionCheckRule[] checkRules { get; private set; }

        public MathExpressionParser(MathExpressionOperator[] operators, (char, char)[] brackets, char[] separators, IMathExpressionCheckRule[] checkRules)
        {
            this.operators = operators;
            this.brackets = brackets;
            this.separators = separators;
            this.checkRules = checkRules;
        }

        public bool Check(string originString, out string errorMessage)
        {
            errorMessage = null;
            foreach (IMathExpressionCheckRule rule in checkRules)
            {
                if (!rule.Check(originString, operators, brackets, separators))
                {
                    errorMessage = rule.GetErrorMessage();
                    return false;
                }
            }

            return true;
        }

        public BinaryTreeNode<string> Parse(string originString)
        {
            BinaryTreeNode<string> rootNode = null;
            BinaryTreeNode<string> previousSignNode = null;
            MathExpressionOperator previousOperator =  null;
            MathExpressionOperator currentOperator = null;

            for (int i = 0; i < originString.Length; ++i)
            {                
                if (MathExpressionUtilities.IsSign(originString[i], operators, out currentOperator))
                {
                    BinaryTreeNode<string> currentSignNode = SignHandle(currentOperator, previousOperator, previousSignNode);

                    if (rootNode == null)
                        rootNode = currentSignNode;
                    else if (previousSignNode == null)
                    {
                        currentSignNode.SetLeftNode(rootNode);
                        rootNode = currentSignNode;
                    }
                    else if (currentSignNode.parentNode == null)
                        rootNode = currentSignNode;

                    previousSignNode = currentSignNode;
                    previousOperator = currentOperator;
                }
                else if (MathExpressionUtilities.IsOpenBracket(originString[i], brackets))
                {
                    BinaryTreeNode<string> bracketsNode = BracketsHandle(originString, ref i);

                    if (rootNode == null)
                        rootNode = bracketsNode;
                    else
                        previousSignNode.SetRightNode(bracketsNode);
                }
                else
                {  
                    BinaryTreeNode<string> numberNode = NumberHandle(originString, ref i);
                    --i;

                    if (rootNode == null)
                        rootNode = numberNode;
                    else
                        previousSignNode.SetRightNode(numberNode);
                }
            }

            return rootNode;
        }

        private BinaryTreeNode<string> NumberHandle(string originString, ref int index)
        {
            int numberStartIndex = index;
            for (; index < originString.Length; ++index)
            {
                if (MathExpressionUtilities.IsSign(originString[index], operators) || MathExpressionUtilities.IsBracket(originString[index], brackets))                                    
                    break;                
            }
            return new BinaryTreeNode<string>(originString.Substring(numberStartIndex, index - numberStartIndex));
        }

        private BinaryTreeNode<string> SignHandle(MathExpressionOperator currentOperator, MathExpressionOperator previousOperator, BinaryTreeNode<string> previousSignNode)
        {
            BinaryTreeNode<string> currentSignNode = new BinaryTreeNode<string>(currentOperator.symbol.ToString());

            if (previousSignNode == null)
                return currentSignNode;

            if (currentOperator.priority > previousOperator.priority)
            {
                currentSignNode.SetLeftNode(previousSignNode.rightNode);
                previousSignNode.SetRightNode(currentSignNode);
            }
            else
            {
                while (currentOperator.priority <= previousOperator.priority)
                {
                    if (previousSignNode.parentNode != null)
                    {
                        previousSignNode = previousSignNode.parentNode;
                        MathExpressionUtilities.IsSign(previousSignNode.data[0], operators, out previousOperator);
                    }
                    else break;
                }

                switch (previousSignNode.nodeSide)
                {
                    case Side.Left:
                        previousSignNode.parentNode.SetLeftNode(currentSignNode);
                        break;

                    case Side.Right:
                        previousSignNode.parentNode.SetRightNode(currentSignNode);
                        break;
                }

                currentSignNode.SetLeftNode(previousSignNode);
            }

            return currentSignNode;
        }

        private BinaryTreeNode<string> BracketsHandle(string originString, ref int index)
        {
            ++index;
            int bracketsClosed = 1;            
            int startIndex = index;

            for (; index < originString.Length; ++index)
            {
                if (MathExpressionUtilities.IsOpenBracket(originString[index], brackets))
                    ++bracketsClosed;

                if (MathExpressionUtilities.IsCloseBracket(originString[index], brackets))
                    --bracketsClosed;

                if (bracketsClosed == 0)
                    break;
            }

            return Parse(originString.Substring(startIndex, index - startIndex));
        }
    }
}